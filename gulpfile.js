var gulp = require("gulp");
var minify = require("gulp-minify");
var uglify = require("gulp-uglify");
var compass = require("gulp-compass");

// Minify et Uglify le JS :

gulp.task("compress", function (cb) {
    gulp.src("script.js")
        .pipe(uglify())
        .pipe(minify())
        .pipe(gulp.dest('dist'))

    cb();
});

// Transforme les scss en css avec compass :

gulp.task("compass", function (cb) {
    gulp.src("./src/*.scss")
        .pipe(compass({
            config_file: "./config.rb",
            css: "stylesheets",
            sass: "sass"
        }))
        .pipe(gulp.dest("app/assets/temp"));

    cb();
});

// Utilise compass à chaque mise à jour des fichiers .scss :

gulp.task("watch", function() {
    gulp.watch("sass/**/*.scss", gulp.series("compass"));

});