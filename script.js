

$("input").change(function () {

    // Série 1 :

    var red1 = $("#R1").val();
    console.log("R : " + red1);

    var green1 = $("#G1").val();
    console.log("G : " + green1);

    var blue1 = $("#B1").val();
    console.log("B : " + blue1);

    var color1 = "rgb(" + red1 + "," + green1 + "," + blue1 + ")";
    console.log(color1);
    $("#couleur1").css("backgroundColor", color1);


    // Série 2 :

    var red2 = $("#R2").val();
    console.log("R : " + red2);

    var green2 = $("#G2").val();
    console.log("G : " + green2);

    var blue2 = $("#B2").val();
    console.log("B : " + blue2);

    var color2 = "rgb(" + red2 + "," + green2 + "," + blue2 + ")";
    console.log(color2);
    $("#couleur2").css("backgroundColor", color2);

    // Gradient :

    $("#gradient").css("background", "linear-gradient(to right, " + color1 + "," + color2 + ")");
});